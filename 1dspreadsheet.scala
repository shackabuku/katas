import scala.io.{StdIn => stdin}
object Solution extends App {
    var lines: List[String] = Nil
    def compose(step: Int): List[String] = stdin.readLine() :: (if (step > 1) compose(step - 1) else Nil)
    def reffer(e: String): Int = e(0) match {
        case '$' => calculate(e.tail.toInt)
        case '_' => 0
        case _ => e.toInt
    }
    def calculate(pos: Int): Int = {
        val work = lines(pos).split(" ")
        val (op, e1, e2) = (work.head, reffer(work(1)), reffer(work(2)))
        op match {
            case "VALUE" => e1
            case "ADD" => lines = lines.updated(pos, "VALUE " + (e1 + e2) + " _"); e1 + e2
            case "SUB" => lines = lines.updated(pos, "VALUE " + (e1 - e2) + " _"); e1 - e2
            case "MULT" => lines = lines.updated(pos, "VALUE " + (e1 * e2) + " _"); e1 * e2
        }
    }
    lines = compose(stdin.readInt())
    lines.zipWithIndex.map(x => calculate(x._2)).foreach(s => println(s))
}
