#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <string.h>

#define DIE(assertion, call_description)				\
	do {								\
		if (assertion) {					\
			fprintf(stderr, "(%s, %d): ",			\
					__FILE__, __LINE__);		\
			perror(call_description);			\
			exit(EXIT_FAILURE);				\
		}							\
	} while (0)

#define VALUE "VALUE"
#define ADD "ADD"
#define SUB "SUB"
#define MULT "MULT"

static char ** read_lines(int n) {
    int i;
    char **lines;
    size_t len = 0;
    ssize_t line_size = 0;

    lines = malloc(n * sizeof(char *));
    for (i = 0; i < n; i++) {
        lines[i] = NULL;
        line_size = getline(&lines[i], &len, stdin);
        DIE(line_size < 7, "Incorrect input");
        if (lines[i][line_size-1] == '\n')
            lines[i][line_size-1] = '\0';
    }

    return lines;
}

static void cleanup(int n, char **lines) {
    int i;

    for (i = 0; i < n; i++)
        free(lines[i]);
    free(lines);
}

static int str2int(char *in) {
    int base = 10;
    char *end;

    DIE((in[0] == '\0' || in[0] == ' '), "Malformed input");

    long l = strtol(in, &end, base);

    DIE((l == 0 && errno == EINVAL), "Inconvertible");
    DIE((l == 0 && errno == ERANGE), "Overflow");

    return l;
}

static int parse_line(int pos, char **lines, int *results) {
    char *line;
    int num1, num2;
    int is_ref1 = 0;
    int is_ref2 = 0;
    char *op1, *op2;
    char *delim = " ";

    if (results[pos] == INT_MIN) {
        line = strdup(lines[pos]);

        op1 = strtok(line, delim);
        DIE(op1 == NULL, "Invalid input line");
        
        op1 = strtok(NULL, delim);
        DIE(op1 == NULL, "First operator not present");
        
        op2 = strtok(NULL, delim);
        DIE(op2 == NULL, "Second operator not present");

        if (op1[0] == '$') {
            is_ref1 = 1;
            op1++;
        }
        num1 = str2int(op1);

        if (is_ref1)
            num1 = parse_line(num1, lines, results);
        
        if (op2[0] == '$') {
            is_ref2 = 1;
            op2++;
        }
        
        if (op2[0] != '_')
            num2 = str2int(op2);

        if (is_ref2)
            num2 = parse_line(num2, lines, results);

        if (strncmp(VALUE, line, strlen(VALUE)) == 0)
            results[pos] = num1;
        else if (strncmp(ADD, line, strlen(ADD)) == 0)
            results[pos] = num1 + num2;
        else if (strncmp(SUB, line, strlen(SUB)) == 0)
            results[pos] = num1 - num2;
        else if (strncmp(MULT, line, strlen(MULT)) == 0)
            results[pos] = num1 * num2;
        else {
            fprintf(stderr, "Incorrect input\n");
            exit(EXIT_FAILURE);
        }
    }

    return results[pos];
}

int main() {
    int i, n;
    char *line = NULL;
    char **lines = NULL;
    int *results;
    size_t len = 0;
    ssize_t line_size = 0;

    line_size = getline(&line, &len, stdin);
    DIE(line_size == 1, "Incorrect input");
    n = str2int(line);
    free(line);

    lines = read_lines(n);

    results = malloc(n * sizeof(int));
    for (i = 0; i < n; i++)
        results[i] = INT_MIN;

    for (i = 0; i < n; i++) {
        parse_line(i, lines, results);
    }

    cleanup(n, lines);

    for (i = 0; i < n; i++) {
        printf("%d\n", results[i]);
    }
    
    return 0;
}
